/*
 * Geef de laatste 2 cijfers uit een getal dat minstens uit 2 cijfers bestaat.
 */
public class H2_EM02 {

	public static void main(String[] args) {
		int getal = Invoer.leesInt("Geef een getal dat minstens uit 2 getallen bestaat: ");
		getal = getal % 100;
		System.out.println(getal);
	}
}
