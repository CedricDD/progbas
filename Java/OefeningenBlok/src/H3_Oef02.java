/*
 * Geef via het toetsenbord het brutoloon van een werknemer in. Gevraagd wordt het jaarlijks vakantiegeld en de jaarlijkse bijdrage te berekenen en af te drukken.
 * Het vakantiegeld is 5% van dit brutoloon. Is dit vakantiegeld minstens 350 euro, dan is jaarlijkse bijdrage gelijk aan 8% van 350 euro.
 * Is dit vakantiegeld kleiner dan 350 euro, dan is de jaarlijkse bijdrage 8% van het vakantiegeld.
 * Druk voor iedere werknemer af: brutoloon, vakantiegeld en jaarlijkse bijdrage.
 * Al deze bedragen worden afgerond op 1 decimaal na de komma.
 */

public class H3_Oef02 {
	public static void main(String[] args) {
		double brLoon = Invoer.leesDouble("Brutoloon Werknemer: ");
		double vakGeld = brLoon * 0.05;
		double jaarBijdrage;
		if (vakGeld >= 350) {
			jaarBijdrage = Math.round(350 * 0.08 * 10) / 10;
		} else {
			jaarBijdrage = Math.round(vakGeld * 0.08 * 10) / 10;
		}
		System.out.println("Brutooon: " + brLoon + "\tVakantiegeld: " + vakGeld + "\tJaarlijkse bijdrage: " + jaarBijdrage);
	}
}
