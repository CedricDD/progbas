/*
 * Wat kost mijn auto? Prijsbewuste personen willen weten hoeveel hun auto echt kost. Achtereen volgens wordt ingevoerd:
 * aantal afgelegde kilometers, verbruik in l per 100 km, prijs van 1l brandstof.
 * Als uitvoer wordt verlangd:
 * de totale kosten per jaar voor het opgegeven aantal kilomter, de kostprijs per km rijden.
 * Beide resultaten wordt afgekapt op 2 decimalen na de komma.
 */

public class H2_E06 {

	public static void main(String[] args) {
		int kilometers = Invoer.leesInt("Kilometers: ");
		double verbruik = Invoer.leesDouble("Verbruik: ");
		double prijsBr = Invoer.leesDouble("Prijs Brandstof: ");
		double prKm = (int) ((verbruik * prijsBr / 100) * 100) / 100.0;
		double prJaar = (int) ((prKm * kilometers) * 100) / 100.0;

		System.out.println(prJaar + "\t" + prKm);
	}
}
