/*
 * Druk een vliegticket af. Naam, bestemming, afstand in km, klasse, het vertrekuur en de vertrekminuten worden ingevoerd.
 * Druk af: naam, bestemming, afstand, klasse, prijs en het tijdstip dat men aanwezig dient te zijn: 1u45 voor vertrek, maaltijd Ja/Nee.
 * Prijsberekening: <1000km = 0.25/km, 1000-2999km = 0.20/km, >2999km = 0.15/km.
 * Klasse : Toerist (T), Charter (C) = -20%, Zaken (Z) = +30%.
 * Voor vluchten vanaf 1000km is er een maaltijd voorzien.
 */
public class H3_E06 {
	public static void main(String[] args) {
		String naam = Invoer.leesString("Naam: ");
		String bestemming = Invoer.leesString("Bestemming: ");
		int afstand = Invoer.leesInt("Afstand: ");
		char klasse = Invoer.leesChar("Klasse: ");
		int vUur = Invoer.leesInt("Vertrekuur: ");
		int vMin = Invoer.leesInt("Vertrekminuten: ");
		double prijs;
		String maaltijd = "Nee";
		if (afstand < 1000) {
			prijs = afstand * 0.25;
		} else {
			maaltijd = "Ja";
			if (afstand < 3000) {
				prijs = afstand * 0.20;
			} else {
				prijs = afstand * 0.15;
			}
		}
		if (klasse == 'T') {
		} else if (klasse == 'C') {
			prijs *= 0.8;
		} else if (klasse == 'Z') {
			prijs *= 1.3;
		}

		if (vMin >= 45) {
			vUur -= 1;
			vMin -= 45;
		} else {
			vMin += 15;
			vUur -= 2;
		}
		if (vUur < 0) {
			vUur += 24;
		}
		System.out.println("Naam: " + naam + "\nBestemming: " + bestemming
				+ "\nAfstand: " + afstand + "\nKlasse: " + klasse + "\nPrijs: "
				+ prijs + "\nMaaltijd: " + maaltijd
				+ "\nGelieve voor dit uur aanwezig te zijn: " + vUur + ":"
				+ vMin);
	}
}
