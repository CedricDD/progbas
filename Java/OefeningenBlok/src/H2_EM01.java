/*
 * Schrijf een programma dat 3 getallen a, b en c inleest. Verplaats de waarde van a naar b, van b naar c en van c naar a (cyclisch permuteren).
 * Druk daarna de waarden a, b en c af.
 */
public class H2_EM01 {
	public static void main(String[] args) {
		int getalA = Invoer.leesInt("Getal 1: ");
		int getalB = Invoer.leesInt("Getal 2: ");
		int getalC = Invoer.leesInt("Getal 3: ");
		int hulpGetal = getalA;
		getalA = getalC;
		getalC = getalB;
		getalB = hulpGetal;
		System.out.println(getalA + "\t" + getalB + "\t" + getalC);
	}
	
}
