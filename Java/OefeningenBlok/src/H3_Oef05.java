/*
 * Een verzekeringsmaatschappij verkoopt levensverzekeringen en berekent de te betalen maandpremie aan de hand van deze tabel:
 * leeftijd		maandpremie per 1250 euro
 * <20			3
 * >=20 <30		8
 * >=30 <40		18
 * >=40 < 60	25
 * >=60			37
 * 
 * De leeftijd van de verzekerde, alsook het te verzekeren bedrag (een veelvoud van 1250) wordt ingegeven.
 * Druk het te betalen bedrag per jaar af. Bv. Iemand van 28 en een bedrag 25000 euro = 1920 euro / jaar.
 */
public class H3_Oef05 {
	public static void main(String[] args) {
		int leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
		int verzBedrag = Invoer.leesInt("Geef het te verzekeren bedrag in: ");
		int factor = verzBedrag / 1250, maandPremie;
		if (leeftijd < 20) {
			maandPremie = factor * 3;
		} else if (leeftijd < 30) {
			maandPremie = factor * 8;
		} else if (leeftijd < 40) {
			maandPremie = factor * 18;
		} else if (leeftijd < 60) {
			maandPremie = factor * 25;
		} else {
			maandPremie = factor * 37;
		}
		System.out.println("Leeftijd: " + leeftijd + "\tTe verzekeren bedrag: " + verzBedrag + "\tJaarpremie: " + maandPremie * 12);
	}
}
