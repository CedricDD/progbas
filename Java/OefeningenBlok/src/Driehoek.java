
public class Driehoek {
	public static void main(String[] args) {
		int grootte = Invoer.leesInt("Grootte: ");
		char teken = Invoer.leesChar("Teken: ");
		for (int i = 1; i <= grootte; i++) {
			for (int j = 1; j <= grootte - i; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print(teken);
			}
			System.out.println();
		}
		
		
	}
}
