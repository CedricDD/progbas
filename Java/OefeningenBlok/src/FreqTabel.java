public class FreqTabel {
	public static void main(String[] args) {
		int[] tabel = new int[10];
		int som = 0, aantal = 0;
		double gem;
		int getal = Invoer.leesInt("Geef een getal in: ");
		while (getal != 0) {
			for (int i = 0; i <= tabel.length - 1; i++) {
				if (i + 1 == getal) {
					tabel[i] += 1;
				}
				som += getal;
				aantal += 1;
			}
			getal = Invoer.leesInt("Geef een getal in: ");
		}
		gem = (double) som / aantal;
		System.out.println("Getal:\tAantal:");
		for (int i = 0; i <= tabel.length - 1; i++) {
			System.out.println(i + 1 + "\t" + tabel[i]);
		}
		System.out.println("\nHet gemiddelde is: " + gem);
	}
}