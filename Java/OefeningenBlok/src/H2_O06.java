/*
 * Wim volgt het eerste jaar toegepaste informatica, hij heeft slechts een zeer beperkt programma, nl 2 opleidingsonderdelen. 
 * Java (7 studiepunten, waarvan 40% op PE, 60% op het examen) en Netwerkbesturingssytemen (3 studiepunten, waarvan  70% op een opdracht, 30% op het examen).
 * Voer de 4 resultaten in via het toetsenbord, telkens op 20 punten.
 * Druk het totaalresultaat af per opleidingsonderdeel en het totaal behaald percentage (hierbij hou je rekening met het aantal studiepunten). 
 */
public class H2_O06 {
	public static void main(String[] args) {
		int javaPE = Invoer.leesInt("Java Pe: ");
		int javaEx = Invoer.leesInt("Java Examen: ");
		int nbsOp = Invoer.leesInt("Netwerken Opdracht: ");
		int nbsEx = Invoer.leesInt("Netwerken Examen: ");
		double javaTot = javaPE * 0.4 + javaEx * 0.6;
		double nbsTot = nbsOp * 0.7 + nbsEx * 0.3;
		double Perc = ((javaTot * 7 + nbsTot * 3) / 10) * 5;
		System.out.println(javaTot + "\t" + nbsTot + "\n");
		System.out.println(Perc);
		
	}
}
