/*
 * Schrijf een programma om de middelste of twee middelste letters van een woord om te wisselen.
 */
public class LettersOmwisselen {

	public static void main(String[] args) {
		String woord = Invoer.leesString("Geef een woord in: ");
		char letter1, letter2;
		int aantal = woord.length();
		if (aantal % 2 == 0) {
			letter1 = woord.charAt(aantal / 2 - 1);
			letter2 = woord.charAt(aantal / 2);
			System.out.println(woord.substring(0, aantal / 2 - 1) + letter2
					+ "" + letter1 + woord.substring(aantal / 2 + 1));
		} else {
			letter1 = woord.charAt(aantal / 2);
			letter2 = woord.charAt(aantal / 2 + 1);
			System.out.println(woord.substring(0, aantal / 2) + letter2 + ""
					+ letter1 + woord.substring(aantal / 2 + 2));
		}

	}

}
