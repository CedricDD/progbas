import java.io.*;

public class ExtraOef04 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer ("H6_extraoefening4.txt");
		int[] score = new int [100];
		int tot = 0, teller = 0, onderGem = 0, ingave, grootst = 0;
		double gem;
		for (int i = 0; i <= 99; i++) {
			for (int j = 0; j <= 4; j++) {
				ingave = a.leesInt();
				score[i] += ingave;
			}
			if (score[i] > grootst) {
				grootst = score[i];
			}
			tot += score[i];
			teller += 1;
		}
		gem = (double) tot / teller;
		for (int i = 0; i <= 99; i++) {
			if (score[i] < gem) {
				onderGem += 1;
			}
		}
		System.out.println("De maximum score is: " + grootst + "\nDe gemiddelde score is: " + gem + "\nAantal deelnemers onder het gemiddelde: " + onderGem);
	}

}
