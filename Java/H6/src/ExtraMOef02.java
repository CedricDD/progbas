/*
 * Duplicaten verwijderen
 */
public class ExtraMOef02 {

	public static void main(String[] args) {
		// in deze oefeningen word verondersteld dat de invoer al op orde staat van klein naar groot
		int a[] = new int[50], tel2 = 0;
		for (int i = 1; i<=10; i++) {
			a[i] = Invoer.leesInt("geef getal "+i);
		}
		a[0] = a[1];
		tel2++;
		for (int i = 1; i<=9; i++) {
			a[tel2] = a[i+1];
			if (a[tel2] != a[tel2-1]) {
				tel2++;
			}
		}
		for (int i = tel2+1; i<=49; i++) {
			a[i] = 0;
		}
		for (int i = 0; i<=tel2-1; i++) {
			System.out.print(a[i]);
		}
	}

}
