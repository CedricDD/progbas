public class ExtraOef05 {
	static double weekloon;
	static int uVorderen, positie;

	public static void main(String[] args) {
		int persNr, tot = 0, totVord = 0, uPerWeek, aantal = 0, aantalP, teller = 0;
		double uurloon, gem;
				int[] cat = new int[5];
		aantalP = Invoer.leesInt("Geef het aantal personeelsleden in: ");
		int[] leden = new int[aantalP];
		double[] loon = new double [aantalP];
		int[] uV = new int [aantalP];
		persNr = Invoer.leesInt("Geef het personeelsnummer in: ");
		while (persNr != 0) {
			uurloon = Invoer.leesDouble("Geef het uurloon in: ");
			uPerWeek = Invoer.leesInt("Geef het aantal gewerkte weekuren in: ");
			loonUren(uurloon, uPerWeek);
			aantal += 1;
			tot += weekloon;
			totVord += uVorderen;
			leden[teller] = persNr;
			loon[teller] = weekloon;
			uV[teller] = uVorderen;
			teller += 1;
			cat[positie] += 1;
			persNr = Invoer.leesInt("Geef het personeelsnummer in: ");
		}
		System.out.println("\nPersoneelsnummer:\tWeekloon:\tUren te vorderen:");
		for (int i = 0; i <= aantalP - 1; i++) {
			System.out.println(leden[i] + "\t\t\t" + loon[i] + "\t\t" + uV[i]);
		}
		System.out.println("\nAantal personeelsleden: " + aantal + "\tTotaal Loon: " + tot + "\tTotaal Uren te vorderen: " + totVord + "\t");
		System.out.println("\nCategorie:\t\tAantal:");
		for (int i = 0; i <= 4; i++) {
			System.out.println("Categorie: " + i + "\t\tAantal: " + cat[i]);
		}
	}

	private static void loonUren(double uurloon, int uPerWeek) {
		int overuren = 0;
		double eersteTien = 10 * uurloon * 1.5;
		double eersteVijftien = eersteTien + (5 * uurloon * 2.0);
		if (uPerWeek > 38) {
			overuren = uPerWeek - 38;
			if (overuren <= 10) {
				weekloon += overuren * uurloon * 1.5;
				positie = 2;
			} else if (overuren <= 15) {
				weekloon += eersteTien + (overuren - 10) * uurloon * 2.0;
				positie = 3;
			} else {
				weekloon += eersteVijftien + (overuren - 15) * uurloon * 1.2;
				uVorderen = overuren - 15;
				positie = 4;
			}
		} else if (uPerWeek == 38) {
			positie = 0;
		} else {
			positie = 1;
		}
		weekloon = 38.0 * uurloon;
	}

}
