/*
 * Schrijf een programma om de som te maken van de elementen van een tabel a. De tabel heeft 20 elementen en zit reeds in het intern geheugen.
 * Maak een tabel aan van gehele getallen waarbij de elementen willekeurige getallen zijn, gelegen tussen -100 en 100 (-100 en 100 niet inbegrepen).
 */
public class Oef01 {

	public static void main(String[] args) {
		int[] a = new int[20];
		int som = 0;
		for (int i = 0; i <= a.length - 1; i++) {
			a[i] = (int) (-100 + (+100 - (-100)) * Math.random());
			som += a[i];
		}
		System.out.println(som);
	}

}
