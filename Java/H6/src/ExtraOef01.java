/*
 * Een examenzittijd bestaat uit 15 vakken. De beoordeling van elk vak wordt uitegedrukt in een geheel getal van 1 tot met 10.
 * Geslaagd = maximaal 2 vakken < 6/10
 * De cijfers worden in een tabel opgeslaan, bepaal of de leerling geslaagd is of niet. 
 */
public class ExtraOef01 {

	public static void main(String[] args) {
		int[] resultaten = new int[15];
		int teller = 0;
		for (int i = 0; i <= 14; i++) {
			resultaten[i] = Invoer
					.leesInt("Geef resultaat " + (i + 1) + " in: ");
			if (resultaten[i] < 6) {
				teller += 1;
			}
		}
		if (teller > 2) {
			System.out.println("Niet geslaagd!");
		} else {
			System.out.println("Geslaagd!");
		}
	}

}
