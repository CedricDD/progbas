/*
 * Bereken de 144 mogelijkde producten van tabel a (12 elementen) met tabel b (12 elementen).
 * Vermenigvuldig het eerste element van a met het eerste element van b, etc.
 * Plaats de 144 resultaten in een nieuwe tabel c.
 */
public class ExtraOef02 {

	public static void main(String[] args) {
		int[] a = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
		int[] b = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120 };
		int[] c = new int[144];
		int teller = 0;
		for (int i = 0; i <= 11; i++) {
			for (int j = 0; j <= 11; j++) {
				c[teller] = a[i] * b[j];
				teller +=1;
			}
		}
		for (int i = 0; i <= c.length - 1; i++) {
			System.out.println(c[i]);
		}
	}

}
