/*
 * Via het toetsenbord worden 30 getallen ingegeven, verdeel de negatieve en positieve getallen over 2 tabellen.
 * Druk de 2 tabellen af en druk ook het het kleinste negatieve getal af.
 */
import java.util.Arrays;
import java.util.Collections;

public class ExtraOef03 {

	public static void main(String[] args) {

		int getal, kleinst = 0, tellerpos = 0, tellerneg = 0, lengte, tellergrootst = 15;
		String tekst;
		int pos[] = new int[30];
		int neg[] = new int[30];
		int regels = 15;
		for (int i = 0; i <= 29; i++) {
			getal = Invoer.leesInt("Geef een getal in");

			if (getal < 0) {
				tellerneg += 1;
				neg[i] = getal;
				if (getal < kleinst) {
					kleinst = getal;
				}
			} else {
				tellerpos += 1;
				pos[i] = getal;
			}
		}
		Arrays.sort(neg);							// sorteer array
		if (tellerpos > tellerneg) {				// bepaal aantal regels om af te drukken
			regels = tellerpos;
		} else {
			regels = tellerneg;
		}
		for (int i = 0; i <= pos.length - 1; i++) {		// maak alle positieve getallen, negatief
			  pos[i] = pos[i] * -1;
			}
		Arrays.sort(pos);							// sorteer array
		for (int i = 0; i <= pos.length - 1; i++) {		// maak alle negatieve weer positief
			  pos[i] = pos[i] * -1;
			}
		System.out.println("POS\t\tNEG");
		for (int i = 0; i <= regels - 1; i++) {
			if (i <  tellerpos) {
				System.out.print(pos[i] + "\t\t");
			} else {
				System.out.print("\t\t");
			}
			if (i <  tellerneg) {
				System.out.print(neg[i] + "\n");
			} else {
				System.out.print("\n");
			}
		}
		System.out.println("\nHet kleinste getal is: " + kleinst);
	}

}