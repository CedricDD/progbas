/*
 * Telkens hij benzine tankt heeft Pieter opgeshreven hoeveel liter bijgevuld werd en hoeveel kilometer hij afgelegd heeft. Maak een programma om het gemiddeld verbruik te berekenen.
 * Er wordt 0 ingegeven om te stoppen met ingeven, druk het gemiddelde af per 100km.
 */
public class Oef06 {

	public static void main(String[] args) {
		int liters, kiloMeters;
		double totLit = 0, totKm = 0, gemVerbruik = 0;
		/*
		 * do { kiloMeters = Invoer.leesInt("Geef het aantal kilometer in: ");
		 * if (kiloMeters != 0) { liters =
		 * Invoer.leesInt("Geef het aantal liter in: "); totLit += liters; totKm
		 * += kiloMeters; }
		 * 
		 * }while (kiloMeters != 0); gemVerbruik = totLit/totKm*100;
		 * System.out.println("Het gemiddelde verbruik per 100km is " +
		 * gemVerbruik + " liter");
		 */

		kiloMeters = Invoer.leesInt("Geef het aantal kilometer in: ");
		while (kiloMeters != 0) {
			liters = Invoer.leesInt("Geef het aantal liter in: ");
			totLit += liters;
			totKm += kiloMeters;
			kiloMeters = Invoer.leesInt("Geef het aantal kilometer in: ");
		}
		if (totKm != 0) {
			gemVerbruik = totLit / totKm * 100;
			System.out.println("Het gemiddelde verbruik per 100km is " + gemVerbruik + " liter");
		}
	}

}
