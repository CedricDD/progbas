/*
 * Geef via het toetsenbord het resultaat op 20 in van 5 vakken van een student.
 * Druk het gemiddelde resultaat af (afronden op 1 decimaal) en het percentage.
 * 
 * Zelfde opgave als 9a, maar voor 3 studenten.
 * Hoeveel studenten zijn geslaagd? Men slaagt vanaf 60%.
 */
public class Oef09b {

	public static void main(String[] args) {
		int resVak, percVakken = 0, totVakken, geslaagd = 0;
		double gemRes;
		String naamStud;
		for (int i = 1; i <= 3; i++) {
			naamStud = Invoer.leesString("Geef de naam in: ");
			for (int j = 1; j <= 5; j++) {
				resVak = Invoer.leesInt("Resultaat op 20: ");
				percVakken += resVak;
			}
			if (percVakken <= 60) {
				geslaagd += 1;
			}
			gemRes = (int) ((double) percVakken / 5 * 10) / 10;
			System.out.print("Gemiddelde: " + gemRes + "\t" + "\t" + "Percentage: " + percVakken + "\n");
			percVakken = 0;
		}
		System.out.println("Aantal geslaagde studenten: " + geslaagd);

	}

}
