/*
 * Er wordt een programma geschreven dat de verkoopscijfers per maand vraagt en het totale verkoopscijfer per jaar berekent en afdrukt.
 */
public class ExtraOef02 {

	public static void main(String[] args) {
		int verkCijfer, jaar = 0;
		for (int i = 1; i <= 12; i++) {
			verkCijfer = Invoer.leesInt("Geef het verkoopscijfer in: ");
			jaar = jaar + verkCijfer;
		}
		System.out.println("De jaarsom is: " + jaar);
	}

}
