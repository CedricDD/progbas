/*
 * Wat is het eindkapitaal van een bepaald beginbedrag, uitgezet op samengestelde intrest gedurende een aantal jaar? Dit wordt ingelezen: Beginkapitaal, intrestvoet, aantal jaren.
 * Gebruik deze formule: eindkapitaal = beginkapitaal * (1 + intrestvoet)^aantal jaren.
 * Het kapitaal na elk jaar wordt afgedrukt, voor de machtsverheffing gebruik je een iteratie.
 */
public class ExtraOef05 {

	public static void main(String[] args) {
		int beginKap, duurJaren;
		double intrestVoet, eindKap = 0;
		beginKap = Invoer.leesInt("Geef het beginkapitaal in: ");
		intrestVoet = Invoer.leesDouble("Geef de intrestvoet in: ");
		duurJaren = Invoer.leesInt("Geef de duur in jaren in: ");
		for (int i = 1; i < duurJaren; i++) {
			eindKap = i * (beginKap * (1 + intrestVoet));
		}
		System.out.println("Het eindkapitaal is: " + eindKap);
	}

}
