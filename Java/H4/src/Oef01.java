/*
 * Lees een rij getallen in en druk de som af. Deze rij eindigt met een negatief getal.
 */
public class Oef01 {

	public static void main(String[] args) {
		int getal, som = 0;
		getal = Invoer.leesInt("Geef een getal in: ");
		while (getal >= 0) {
			som = som + getal;
			getal = Invoer.leesInt("Geef een getal in: ");			
		}
		System.out.println("De som is: " + som); //
	}

}
