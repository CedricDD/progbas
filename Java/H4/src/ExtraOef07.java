/*
 * Wat is de totale som van de verkoopscijfers uit oefening 2 gedurende de maanden van 10 opeenvolgende jaren. Gebruik H4_extraoefening7.txt
 */
import java.io.*;

public class ExtraOef07 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H4_extraoefening7.txt");
		int verkCijfer, jaar = 0;
		for (int i = 1; i <= 10; i++) {
			for (int j = 1; j <= 12; j++) {
				verkCijfer = a.leesInt();
				jaar = jaar + verkCijfer;
			}
			System.out.println("De totale som is: " + jaar);
		}

	}

}
