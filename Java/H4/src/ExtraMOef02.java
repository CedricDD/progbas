/*
 * Maak een programma dat voor 1 week de gewerkte tijden van op uurbasis werkende arbeiders in een fabriek berekent.
 * Voor iedere arbeider worden een werknummer en de kloktijden ingevoerd (begin en eindtijd voor 5 dagen).
 * De tijden wordt uitgedrukt in uren en decimalen van uren. Half zes 's avonds wordt dus 17.50.
 * Per dag wordt de gewerkte tijd berekend, als het verschil negatief is omdat de arbeider tot na middernacht gewerkt heeft, moet dit gecorrigeerd worden door er 24 bij op te tellen.
 * Voor iedere werknemer wordt het werknummer en het urentotaal afgedrukt, het aantal werknummers wordt op voorhand ingegeven.
 */
public class ExtraMOef02 {

	public static void main(String[] args) {
		int werkNr = 0, aantWerkN;
		double klokBegin, klokEinde, dagTijd, totTijd = 0;
		aantWerkN = Invoer.leesInt("Geef het aantal werknemers in: ");
		for (int i = 1; i <= aantWerkN; i++) {
			werkNr = Invoer.leesInt("Geef het werknummer in: ");
			for (int dag = 1; dag <= 5; dag++) {
				klokBegin = Invoer.leesDouble("Geef de begintijd in: ");
				klokEinde = Invoer.leesDouble("Geef de eindtijd in: ");
				dagTijd = klokEinde - klokBegin;
				if (dagTijd < 0) {
					dagTijd += 24;
				}
				totTijd += dagTijd;
				}
			System.out.println("Werknummer: " + werkNr + "\tTotale tijd: " + totTijd);
		}

	}

}
