/*
 * Volgende info over personeelsleden wordt ingevoerd:
 * personeelsnummer, geslacht (0=vrouw, 1=man), leeftijd, brutoloon, diploma (1=unief, 2=HOLT, 3=HOKT, 4=Humaniora).
 * De invoer stopt als er voor het personeelsnummer 0 wordt ingevoerd, doe een invoercontrole voor het geslacht.
 * Druk het aantal mannelijke personen met een universitaire graad af, die ouder zijn dan 34, ofwel een loon hebben van 1800 euro of meer.
 * Druk het aantal vrouwen die jonger zijn dan 25 af.
 * Gebruik H4_oefening12.txt
 * Als er een fout geslacht gelezen wordt, moet deze via het toetsenbord ingegeven worden.
 */
import java.io.*;

public class Oef12b {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H4_Oefening12.txt");
		int persNummer, leeftijd, brutoloon, diploma, geslacht, gevraagd1 = 0, gevraagd2 = 0;
		persNummer = a.leesInt();
		while (persNummer != 0) {
			geslacht = a.leesInt();
			leeftijd = a.leesInt();
			brutoloon = a.leesInt();
			diploma = a.leesInt();
			if (geslacht == 1) {
				if (diploma == 1 && (leeftijd > 34 || brutoloon > 1800)) {
					gevraagd1 += 1;
				}
			} else if (geslacht == 0) {
				if (leeftijd < 25) {
					gevraagd2 += 1;
				}
			} else {
				geslacht = Invoer
						.leesInt("Geef het geslacht in (0 = vrouw, 1 = man): ");
			}
			persNummer = a.leesInt();
		}
		System.out
				.println("Mannen ouder dan 34, brutoloon hoger dan 1800 euro en Universitair diploma: "
						+ gevraagd1);
		System.out.println("Vrouwen jonger dan 25: " + gevraagd2);

	}

}

// geslacht is geen boolean omdat de BestandsLezer blijkbaar geen boolean
// inleest (enkel regel, int, double en float) -> int