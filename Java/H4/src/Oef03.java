/*
 * Lees twee getallen in, maak de som en het verschil en druk deze af. Herhaal dit tot de twee getallen die ingegeven worden beide nul zijn.
 */
public class Oef03 {
 
        public static void main(String[] args) {
                int getal1, getal2, som = 0, verschil = 0;
 
                getal1 = Invoer.leesInt("Geef het eerste getal in: ");
                getal2 = Invoer.leesInt("Geef het tweede getal in: ");
        while(!(getal1 == 0 && getal2 == 0)) {
                        som = getal1 + getal2;
                        verschil = getal1 - getal2;
                        System.out.println("De som is: " + som + ", het verschil is: " + verschil);
                        getal1 = Invoer.leesInt("Geef het eerste getal in: ");
                        getal2 = Invoer.leesInt("Geef het tweede getal in: ");
        }
                System.out.println("Beide getallen mogen niet nul zijn!");
        }
 
}