/*
 * Geef via het toetsenbord het resultaat op 20 in van 5 vakken van een student.
 * Druk het gemiddelde resultaat af (afronden op 1 decimaal) en het percentage.
 */
public class Oef09a {

	public static void main(String[] args) {
		int resVak, percVakken = 0, totVakken;
		double gemRes;
		for (int i = 1; i <= 5; i++) {
			resVak = Invoer
					.leesInt("Geef het resultaat van het vak op 20 in: ");
			percVakken += resVak;
		}
		gemRes = (int) ((double) percVakken / 5 * 10) / 10;
		System.out.println("Gemiddelde: " + gemRes + "\t" + "\t"
				+ "Percentage: " + percVakken);

	}

}