/*
 * Lees een getal a in, bereken a^10 (door gebruik te maken van een iteratie) en druk deze af.
 */
public class ExtraOef01 {

	public static void main(String[] args) {
			int a, macht = 1;
			a = Invoer.leesInt("Geef een getal in: ");
			for (int i = 1; i <= 10; i++) {
				 macht *= a;
			}
			System.out.println("De 10de macht van " + a + " is " + macht);
			System.out.println(Math.pow(15,10));
	}

}