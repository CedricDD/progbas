/*
 * Een trainingscentrum voor managers heeft het volgende criterium tot slagen. Indien het gemiddelde op de 3 examens beneden de 70% is, dan faalt de manager.
 * In alle andere gevallen slaagt hij. De naam en de resultaten van de 3 examens (op 100) worden ingevoerd, de invoer stopt als er xx of XX als naam ingelezen wordt.
 * Druk het volgende af, het gemiddelde wordt afgerond op 1 decimaal.
 * NAAM		TEST1		TEST2		TEST3		GEMIDDELDE		SLAAGT/FAALT
 * Jans An	40			60			20			40				Faalt
 * etc
 * Er slaagden 53.7% van de 286 deelnemers.
 * Gebruik H4_oefeningen11.txt
 */
import java.io.*;

public class Oef11 {

	public static void main(String[] args) throws IOException {
		BestandsLezer a = new BestandsLezer("H4_Oefening11.txt");
		String naamMan, geslaagd;
		int test1 = 0, test2 = 0, test3 = 0, som = 0;
		double gem;
		System.out.println("Naam" + "\t" + "\t" + "Test 1" + "\t" + "\t"
				+ "Test 2" + "\t" + "\t" + "Test 3" + "\t" + "\t"
				+ "Gemiddelde" + "\t" + "Slaagt/Faalt" + "\n");
		naamMan = a.leesRegel();
		while (!(naamMan.equals("xx")||naamMan.equals("XX"))) {
			for (int i = 1; i <= 3; i++) {
				if (test1 == 0) {
					test1 = a.leesInt();
				} else if (test2 == 0) {
					test2 = a.leesInt();
				} else {
					test3 = a.leesInt();
				}
				som = test1 + test2 + test3;
			}
			gem = ((int) ((som / 3.0) * 10 + 0.5)) / 10.0;
			if (gem < 70) {
				geslaagd = "Faalt";
			} else {
				geslaagd = "Slaagt";
			}
			System.out.println(naamMan + "\t" + test1 + "\t" + "\t" + test2
					+ "\t" + "\t" + test3 + "\t" + "\t" + gem + "\t" + "\t"
					+ geslaagd);
			test1 = 0;
			test2 = 0;
			test3 = 0;
			naamMan = a.leesRegel();
		}

	}

}