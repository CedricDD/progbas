/*
 * Geef een gelbedrag in, hoeveel briefjes van 50 euro kan je hiermee maken?
 */

public class ExtraMOef03 {

	public static void main(String[] args) {
		int geldBedrag, briefjes50;
		geldBedrag = Invoer.leesInt("Geef een bedrag in euro in: ");
		briefjes50 = geldBedrag / 50; // gehele deling, rest wordt afgekapt.
		System.out.println("Van het opgegeven bedrag kan je " + briefjes50 + " briefjes van 50 maken");
	}
}
