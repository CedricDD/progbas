/*
 * U geeft via het toetsenbord het aantal graden Celcius in, de computer bertekent het aantal graden Fahrenheit.
 * Dit aantal (afgekapt op 1 decimaal na de komma) wordt op het scherm afgedrukt. De formule is: F=(9/5)*C+32.
 */

public class Oef03 {

	public static void main(String[] args) {
		double gradenCel, gradenFahr;
		System.out.println("Geef het aantal graden in Celsius in: ");
		gradenCel = Invoer.leesDouble();
		gradenFahr = 9.0/5 * gradenCel + 32;
		gradenFahr = gradenFahr * 10;
		gradenFahr = (int) gradenFahr;
//		gradenFahr = (int) (gradenFahr * 10) ; // op 1 lijn ipv 2 dus
		gradenFahr = gradenFahr / 10;
		System.out.println("Het aantal graden in Fahrenheit is: " + gradenFahr);	
	}

}
