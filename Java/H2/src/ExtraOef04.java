/*
 * De diameter van een fietswiel wordt meestal opgegeven in inches (1 inch = 0.025m).
 * Bepaal de omwenteling van een wiel in meters uitgedrukt, bij een opgegeven diameter in inches en druk het bekomen resultaat af.
 * Voorbeeld: Diameter = 16 inches, afgelegde weg = 16 * PI. Afgelegde weg in meters = 16 * PI * 0.025.
 */

public class ExtraOef04 {

	public static void main(String[] args) {
		int diameterInches;
		double omtrek;
		final double INCHTOCM = 0.0025;
		diameterInches = Invoer.leesInt("Geef de diameter in (inches): ");
		omtrek = diameterInches * 3.14 * INCHTOCM;
		System.out.println("De omtrek of omwenteling is: " + omtrek);
	}

}
