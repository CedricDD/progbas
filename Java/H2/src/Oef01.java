/*
 * De toegangsprijs voor een dierentuin bedraagt 11 euro voor volwassenen en 6 euro voor kinderen onder 12de jaar.
 * Maak een programma dat het aantal volwassenen en kinderen inleest via het toetsenbord en dat de totale prijs betekent en afdrukt.
 */

public class Oef01 {

	public static void main(String[] args) {
		int aantalVolw, aantalKind, totalePrijs;
		System.out.println("Geef het aantal volwassenen in: ");
		aantalVolw = Invoer.leesInt();
		System.out.println("Geef het aantal kinderen in: ");
		aantalKind = Invoer.leesInt();
		totalePrijs = aantalVolw * 11 + aantalKind * 6;
		System.out.println("De totale prijs is: " + totalePrijs);
	}

}
