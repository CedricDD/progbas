/*
 * De telefoonmaatschappij rekent aan haar klaten tweemaandelijks een vast bedrag van 20 euro (aansluiting, huur, onderhoud).
 * Een telefoongesprek binnen Belgie kost 12 eurocent, ongeacht de duur van het gesprek.
 * Een telefoongesprek naar het buitenland kost 50 eurocent per begonnen minuut,
 * In deze tarieven is geen BTW inbegrepen.
 * Geef het via het toetsenbord het aantal Belgische gesprekken en het aantal minuten naar het buiteland in.
 * Het programma berekent hoeveel u moet betalen, het BTW-percentage is 21%.
 */

public class ExtraOef01 {

	public static void main(String[] args) {
		int gesprekkenBel, minutenBui, vasteKost = 20;
		double totaalBel, totaalBui, totaalKost;
		final double btw = 0.21;
		gesprekkenBel = Invoer.leesInt("Geef het aantal gesprekken in Belgie in:");
		minutenBui = Invoer.leesInt("Geef het aantal minuten van het buitenland in: ");
		totaalBel = gesprekkenBel * 0.12;
		totaalBui = minutenBui * 0.5;
		totaalKost = totaalBel + totaalBui + vasteKost * (1 + btw);
		System.out.println("De totale kost bedraagt: " + totaalKost + " euro");
	}
}