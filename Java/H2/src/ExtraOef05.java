/*
 * Hoeveel omwentelingen zal een fietswiel moetem maken om een opgegeven aantal m af te leggen.
 * De diameter van het fietswiel wordt in inches opgegeven (zie vorige opgave).
 * Volgende gegevens worden ingelezen: diameter fietswiel en de af te leggen afstand.
 */

public class ExtraOef05 {

	public static void main(String[] args) {
		int diameterInches, afstandMeters;
		double omwentelingen;
		final double INCHTOCM = 0.025;
		diameterInches = Invoer.leesInt("Geef de diameter in: ");
		afstandMeters = Invoer.leesInt("Geef de af te leggen afstand in: ");
		omwentelingen = afstandMeters / (diameterInches * 3.14 * INCHTOCM);
		System.out.println("Om " + afstandMeters + "m af te leggen zijn er " + omwentelingen + " omwentelingen nodig");
	}

}
