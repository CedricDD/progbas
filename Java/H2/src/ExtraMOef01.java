/*
 * Schrijf een programma dat 3 getallen a, b en c inleest. Verplaats de waarde van a naar b, van b naar c en van c naar a (cyclisch permuteren).
 * Druk daarna de waarden a, b en c af.
 */

public class ExtraMOef01 {

	public static void main(String[] args) {
		int getalA, getalB, getalC, opslagInt;
		getalA = Invoer.leesInt("Geef het getal a in: ");
		getalB = Invoer.leesInt("Geef het getal b in: ");
		getalC = Invoer.leesInt("Geef het getal c in: ");
		opslagInt = getalA;
		getalA = getalC;
		getalC = getalB;
		getalB = opslagInt;
		System.out.println("Getal a is " + getalA + ", getal b is " + getalB + " en getal c is " + getalC);
	}

}
