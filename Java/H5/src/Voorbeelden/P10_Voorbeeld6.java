package Voorbeelden;
/*
 Schrijf een programma om de middelste of 2 middelste letters van een woord in hoofdletters om te zetten. 
 */
public class P10_Voorbeeld6 {

	public static void main(String[] args) {
		String woord;
		int lengte, evenoneven;
		woord = Invoer.leesString("Geef een woord in: ");
		lengte = woord.length();
		evenoneven = lengte % 2; // rest 0 = even, rest 1 = oneven
		if (evenoneven == 0) {
			String letter1 = (Character.toUpperCase(woord.charAt(lengte / 2 - 1))) + "";
			String letter2 = (Character.toUpperCase(woord.charAt(lengte / 2)))
					+ "";
			System.out.println(woord.substring(0, lengte / 2 - 1) + letter1 + letter2 + woord.substring(lengte / 2 + 1, lengte));
		} else {
			String letter = (Character.toUpperCase(woord.charAt(lengte / 2)))
					+ "";
			System.out.println(woord.substring(0, lengte / 2) + letter + woord.substring(lengte / 2 + 1, lengte));
		}

	}

}