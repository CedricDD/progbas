package Voorbeelden;
/*
 Schrijf een programma om te controleren of een artikelcode juist is. De artikelcode moet bestaan uit 2 letters gevolgd door 2 cijfers.
 Druk een gepaste foutmelding af. 
 */

public class P10_Voorbeeld4 {

	public static void main(String[] args) {
		String artikelCode;
		boolean char1, char2, number1, number2;
		artikelCode = Invoer
				.leesString("Geef de artikelcode in (2 letters, 2 cijfers): ");
		if (artikelCode.length() == 4) {
			char1 = Character.isLetter(artikelCode.charAt(0));
			char2 = Character.isLetter(artikelCode.charAt(1));
			number1 = Character.isDigit(artikelCode.charAt(2));
			number2 = Character.isDigit(artikelCode.charAt(3));
			if (!(char1 && char2 && number1 && number2)) {
				System.out.println("De artikelcode is foutief ingegeven!");
			} else {
				System.out.println("De artikelcode is correct ingeven!");
			}
		} else {
			System.out.println("De artikelcode is foutief ingegeven!");
		}
	}
}
