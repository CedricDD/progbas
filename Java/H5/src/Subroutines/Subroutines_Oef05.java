package Subroutines;

public class Subroutines_Oef05 {
	static int totZwaar = 0, boete;

	public static void main(String[] args) {
		String naam;
		int type, toestand, moment, teller = 0;
		double perc;
		naam = Invoer.leesString("Geef de naam in: ");
		while (!naam.equals("/")) {
			int positie = naam.indexOf(" ");
			String voornaam = naam.substring(positie + 1, positie + 2);
			String achternaam = naam.substring(0, positie);
			type = Invoer.leesInt("Geef het type in (0=zwaar, 1=licht):");
			toestand = Invoer
					.leesInt("Geef de toestand in (0=nuchter, 1=niet nuchter): ");
			moment = Invoer
					.leesInt("Geef het moment in (0=piekperiode, 1=rustig): ");
			berekenBoete(naam, type, toestand, moment);
			System.out.println("Naam: " + voornaam + ". " + achternaam + "\tBoete: " + boete);
			teller += 1;
			naam = Invoer.leesString("Geef de naam in: ");
		}
		perc = (double) totZwaar / teller * 100;
		System.out.println("Percentage zware overtreders: " + perc);
	}

	private static void berekenBoete(String naam, int type, int toestand,
			int moment) {
		if (type == 0) {
			totZwaar += 1;
			if (toestand == 0) {
				if (moment == 0) {
					boete = 500;
				} else {
					boete = 250;
				}
			} else if (moment == 0) {
				boete = 1000;
			} else {
				boete = 500;
			}
		} else if (toestand == 0) {
			boete = 50;
		} else if (moment == 0) {
			boete = 1000;
		} else {
			boete = 500;
		}
	}

}