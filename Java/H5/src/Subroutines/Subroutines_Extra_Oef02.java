package Subroutines;
public class Subroutines_Extra_Oef02 {

	public static void main(String[] args) {
		int aantVolw, aantKind, sterren;
		char kindCode;
		String hotelCode;
		hotelCode = Invoer.leesString("Geef de hotelcode in: ");
		while (!hotelCode.equals("/")) {
			aantVolw = Invoer.leesInt("Geef het aantal volwassenen in: ");
			aantKind = Invoer.leesInt("Geef het aantal kinderen in: ");
			kindCode = Invoer.leesChar("Geef de kindercode in: ");
			sterren = Invoer.leesInt("Geef het aantal sterren in: ");
			berekenPrijs(aantVolw, aantKind, hotelCode, kindCode, sterren);
			hotelCode = Invoer.leesString("Geef de hotelcode in: ");
		}

	}

	private static void berekenPrijs(int aantVolw, int aantKind,
			String hotelCode, char kindCode, int sterren) {
		int prijsVolw = 0, prijsKind = 0, totPrijs;
		if (kindCode != 'A') {
			if (!(hotelCode.substring(0, 2).equals("HI"))) {
				prijsVolw = aantVolw * 70;
			} else {
				switch (sterren) {
				case 5:
					prijsVolw = aantVolw * 60;
				case 4:
					prijsVolw = aantVolw * 60;
					break;
				case 3:
					if (hotelCode.substring(0, 2).equals("BR")
							|| hotelCode.substring(0, 2).equals("AN")) {
						prijsVolw = aantVolw * 60;
					} else {
						prijsVolw = aantVolw * 56;
						prijsKind = 0;
					}
					break;
				case 2:
					prijsVolw = aantVolw * 48;
					prijsKind = 0;
					break;
				case 1:
					prijsVolw = aantVolw * 48;
					prijsKind = 0;
					break;
				}
			}
		} else {
			prijsKind = (prijsVolw / aantVolw) * aantKind;
		}
		totPrijs = prijsVolw + prijsKind;
		System.out.println("Hotelcode: " + hotelCode + "\tSterren: " + sterren
				+ "\tPrijs Volwassenen: " + prijsVolw + "\tPrijs Kinderen: "
				+ prijsKind + "\tTotaalprijs: " + totPrijs);

	}
}
