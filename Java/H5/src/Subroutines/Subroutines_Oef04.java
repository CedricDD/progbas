package Subroutines;
public class Subroutines_Oef04 {
	static int teller = 0, uren, minuten;

	public static void main(String[] args) {
		int topBez = 0, inU, inM, uitU, uitM, duur, totTijd = 0;
		inU = Invoer.leesInt("Geef het IN-uur in: ");
		while (inU != 0) {
			inM = Invoer.leesInt("Geef de IN-minuten in: ");
			uitU = Invoer.leesInt("Geef het UIT-uur in: ");
			uitM = Invoer.leesInt("Geef de UIT-minuten in: ");
			duur = berekenDuur(inU, inM, uitU, uitM);
			if (duur > 60) {
				topBez += 1;
			}
			totTijd += duur;
			teller += 1;
			inU = Invoer.leesInt("Geef het IN-uur in: ");
		}
		int gemDuur = totTijd / teller;
		zetOm(gemDuur);
		System.out.println("Bezoekers meer dan 1 uur aanwezig: " + topBez);
		System.out.println("Gemiddelde aanwezigheidsduur: " + uren
				+ " uren en " + minuten + " minuten");

	}

	private static int berekenDuur(int inU, int inM, int uitU, int uitM) {
		int in, uit;
		in = inU * 60 + inM;
		uit = uitU * 60 + uitM;		
		return uit - in;
	}

	private static void zetOm(int gemDuur) {
		uren = (int) gemDuur / 60;
		minuten = (int) gemDuur % 60;
	}
}