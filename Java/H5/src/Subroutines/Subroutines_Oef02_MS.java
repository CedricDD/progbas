package Subroutines;
public class Subroutines_Oef02_MS {

	public static void main(String[] args) {
		int breedte;
		char teken;
		breedte = Invoer.leesInt("Geef de breedte in: ");
		teken = Invoer.leesChar("Geef het teken in: ");
		tekenDriehoek(breedte, teken);
	}
	
	public static void tekenDriehoek(int breedte,char teken) {
		int tellerSpatie, tellerTeken = 1;
		tellerSpatie = breedte - 1;
		for (int i = 1; i <= breedte; i++) {
			for (int j = 1; j <= tellerSpatie; j++) {
				System.out.print(" ");
			}
			for (int k = 1; k <= tellerTeken; k++) {
				System.out.print(teken);
			}
			tellerSpatie -= 1;
			tellerTeken += 1;
			System.out.println();
		}
	}
}