/*
 * Schrijf een programma voor het berekenen van het jaarloon van vertegenwoordigers van een firma. Dit wordt ingegeven:
 * vertegenwoordigersnummer (4 cijfers), verkocht bedrag. De invoer stopt bij vertegenwoordigersnummer = 9
 * De lonen wordt als volgt berekend:
 * Vast gedeelte = 25000 euro
 * Variabel gedeelte = 5% van het vast gedeelte als dit bedrag < 50000, 10% indien bedrag >=50000 en < 75000. 15% indien bedrag >=75000
 * De berekening gebeurt in een functie. Per vertegenwoordiger wordt zijn nummer afgedrukt, het verkocht bedrag en zijn loon.
 * Wat is het percentage dat voor meer dan 75000 euro verkocht heeft? Druk dit af. 
 */
package Functies;
public class Functies_Oef03 {
	static int topVerk = 0;

	public static void main(String[] args) {
		int vertNummer, controleVertN, percVerk = 0, verkBedrag;
		double perc = 0;
		double jaarloon;
		vertNummer = Invoer.leesInt("Geef het vertegenwoordigersnummer in: ");
		while (!(vertNummer >= 9000 && vertNummer < 10000)) {
			verkBedrag = Invoer.leesInt("Geef het verkochte bedrag in : ");
			jaarloon = berekenLoon(verkBedrag);
			percVerk += 1;
			System.out.println("Vertegenwoordigersnummer: " + vertNummer + "\tVerkocht bedrag: " + verkBedrag + "\tJaarloon: " + jaarloon);
			vertNummer = Invoer.leesInt("Geef het vertegenwoordigersnummer in: ");
		}
		if (topVerk == 0) {
			System.out.println("Percentage verkopers boven de 75000 euro: " + 0);
		} else {
			perc = (int)((double)topVerk / percVerk * 100);	
		}
		System.out.println("Percentage verkopers boven de 75000 euro: " + perc);

	}

	public static double berekenLoon(int verkBedrag) {
		double var;
		if (verkBedrag <= 50000) {
			var = 0.05;
		} else if (verkBedrag <= 75000) {
			var = 0.10;
		} else {
			var = 0.15;
			topVerk += 1;
		}
		return 25000 + var * verkBedrag;
	}

}
