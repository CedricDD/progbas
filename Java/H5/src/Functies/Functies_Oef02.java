/*
 * Schrijf een programma om het lidgeld voor een tennisclub te berekenen. Voor elk lid worden deze gegevens ingegeven:
 * naam, leeftijd, aantal kinderen ten laste, inkomen en aansluitingsjaar. De invoer stopt wanneer er voor naam x of X ingegeven wordt.
 * Per lid moet de naam en het lidgeld afgedrukt worden.
 * Het lidgeld wordt zo berekend in een functie:
 * basisbedrag = 100 euro per jaar
 * leeftijd > 60 = -15 euro
 * leden met kinderen = -7.5 euro per kind (max -35 euro)
 * leden die meer dan 20 jaar lid zijn = -12.5 euro
 * leden met een inkomen < 7500 = -25 euro.
 * De verminderingen mogen gecumuleerd worden, maar het minimum lidgeld is 50 euro.
 * Gebruik H5_oefeningen2.txt
 */
package Functies;
import java.util.Calendar;

public class Functies_Oef02 {

	public static void main(String[] args) {
		String naam;
		int leeftijd, aantKind, inkomen, aansluitJaar;
		double lidgeld;
		naam = Invoer.leesString("Geef de naam in: ");
		while (!naam.equals("x") && !naam.equals("X")) {
			leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
			aantKind = Invoer.leesInt("Geef het aantal kinderen in: ");
			inkomen = Invoer.leesInt("Geef het inkomen in: ");
			aansluitJaar = Invoer.leesInt("Geef het aansluitjaar in: ");
			lidgeld = berekenKost(leeftijd, aantKind, inkomen, aansluitJaar);
			System.out.println("Naam: " + naam + "\tLidgeld: " + lidgeld);
			naam = Invoer.leesString("Geef de naam in: ");
		}

	}

	private static double berekenKost(int leeftijd, int aantKind, int inkomen,
			int aansluitJaar) {
		int kost = 100;
		if (leeftijd > 60) {
			kost -= 15;
		}
		if (aantKind * 7.5 <= 35) {
			kost -= 7.5 * aantKind;
		} else {
			kost -= 35;
		}
		if (Calendar.getInstance().get(Calendar.YEAR) - aansluitJaar < 20) {
			kost -= 12.5;
		}
		if (inkomen < 7500) {
			kost -= 25;
		}
		if (kost < 50) {
			kost = 50;
		}
		return kost;
	}

}
