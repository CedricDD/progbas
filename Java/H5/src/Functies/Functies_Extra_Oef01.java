/*
 * In een bibliotheek wordt een programma gebruikt om te bepalen hoe groot de boeite is die betaald moet worden.
 * Per gebruiker geeft men zijn naam (stop bij xx), het aantal boeken en het aantal dagen dat de termijn overschreden is.
 * De boete is 0.07 euro per dag, bovendien wordt er vanaf de 45ste dag automatisch een brief verstuurd die 0.62 euro kost.
 * De berekening gebeurt in een functie, per persoon wordt de boete afgedrukt.
 * Hoeveel brieven werden er verstuurd?
 */
package Functies;
public class Functies_Extra_Oef01 {

	public static void main(String[] args) {
		String naam;
		int aantBoeken, aantDagen;
		double totBoete;
		naam = Invoer.leesString("Geef de naam in: ");
		while (!naam.equals("xx")) {
			aantBoeken = Invoer.leesInt("Geef het aantal boeken in: ");
			aantDagen = Invoer.leesInt("Geef het aantal dagen in: ");
			totBoete = berekenBoete(aantBoeken, aantDagen);
			System.out.println("Naam: " + naam + "\tTotale boete: " + totBoete);
			naam = Invoer.leesString("Geef de naam in: ");
		}

	}

	private static double berekenBoete(int aantBoeken, int aantDagen) {
		int aantBrieven = 0;
		double boete = 0;
		for (int i = 1; i <= aantBoeken; i++) {
			if (aantDagen >= 45) {
				boete += aantDagen * 0.07 + 0.62;
				aantBrieven += 1;
			} else {
				boete += aantDagen * 0.07;
			}
		}
		return boete;
	}
}
