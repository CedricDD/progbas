/*
 * Een bankbediende wenst snel te kunnen uitrekenen hoeveel de waarde van een bepaald bedrag in euro is in US Dollar.
 * Aan het begin van het programma wordt de waarde van euro tv USD ingegeven. Bv. 1 euro = 1.28 USD.
 * Vervolgens worden de bedragen in euro ingevoerd, waarna de waarde in USD wordt getoond.
 * De omzetting gebeurt in een functie, het programma eindigt als er 0 voor het bedrag in euro wordt ingevoerd.
 */
package Functies;
public class Functies_Oef01 {

	public static void main(String[] args) {
		double bedragEuro, koers, dollar;
		koers = Invoer.leesDouble("Geef de koers in: ");
		bedragEuro = Invoer.leesDouble("Geef het bedrag in Euro in: ");
		while (bedragEuro != 0) {
			dollar = dollarConversie(bedragEuro, koers);
			System.out.println("Het bedrag in dollar is: " + dollar + "$");
			bedragEuro = Invoer.leesDouble("\nGeef het bedrag in Euro in:");
		}
	}

	static double dollarConversie(double bedrag, double factor) {
		double usd;
		usd = bedrag * factor;
		return usd;
	}

}