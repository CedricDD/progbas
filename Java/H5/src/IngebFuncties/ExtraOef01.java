/*
 * Schrijf een programma om PI * sqrt(x^4 + y^4) te berekenen, x en y zijn reeele getallen.
 * Rond het resultaat af op 1 decimaal, gebruik de ingebouwde functies.
 */
package IngebFuncties;

import Functies.Invoer;

public class ExtraOef01 {

	public static void main(String[] args) {
		double x, y, res;
		x = Invoer.leesFloat("Geef x in: ");
		y = Invoer.leesFloat("Geef y in: ");
		res = (Math.round(((Math.PI * (Math.sqrt(Math.pow(x,4) + Math.pow(y, 4))))) * 10.0))/10.0;
		System.out.println("Het resultaat is: " + res);
	}

}
