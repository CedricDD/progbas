/*
 * Maak van 2 tekstvariabelen, 1 tekstvariabele. 
 * Neem de eerste 4 letters van tekst 1 in hoofdletters (indien deze er minder dan 4 bevat, vul aan met *)
 * Neem de eerste 4 letters van tekst 2 in kleine letters (indien deze er minder dan 4 bevat, vul aan met +)
 */
package IngebFuncties;

import Subroutines.Invoer;

public class ExtraOef06 {
	public static void main(String[] args) {
		String tekst1 = Invoer.leesString("Tekst 1: ");
		String tekst2 = Invoer.leesString("Tekst 2: ");
		String plusses = "";
		String stars = "";
		int aantal1 = tekst1.length();
		int aantal2 = tekst2.length();
		if (aantal1 >= 4) {
			tekst1 = tekst1.substring(0, 4).toUpperCase();
		} else {
			for (int i = 1; i <= 4 - aantal1; i++) {
				stars += "*";
			}
			tekst1 = tekst1.toUpperCase() + stars;
		}
		if (aantal2 >= 4) {
			tekst2 = tekst2.substring(0, 4).toLowerCase();
		} else {
			for (int i = 1; i <= 4 - aantal1; i++) {
				plusses += "+";
			}
			tekst2 = tekst2.toLowerCase() + plusses;
		}
		System.out.println(tekst1 + tekst2);
	}

}
