/*
 * Kijk hoe vaak een letter voorkomt in een tekst.
 */
package IngebFuncties;

import Subroutines.Invoer;

public class ExtraOef03 {

	public static void main(String[] args) {
		String tekst;
		char letter;
		int teller = 0;
		tekst = Invoer.leesString("Geef een tekst in: ");
		letter = Invoer.leesChar("Geef een letter in: ");
		for (int i = 0; i <= tekst.length() - 1; i++) {
			if (tekst.charAt(i) == letter) {
				teller += 1;
			}
		}
		System.out.println("De letter " + letter + " komt " + teller + " keer voor");

	}

}
