/*
 * Kijk of het aantal tekens in een tekst even of oneven is. 
 * Oneven = eerste letter in kleine letters afdrukken.
 * Even = laatste letter in hoofdletters afdrukken.
 */

package IngebFuncties;

import Functies.Invoer;

public class Oef03 {

	public static void main(String[] args) {
		String tekst;
		int lengte;
		char kar;
		tekst = Invoer.leesString("Geef een tekst in: ");
		lengte = tekst.length();
		if (lengte % 2 == 0) {
			kar = tekst.charAt(lengte - 1);
			kar = Character.toUpperCase(kar);
			System.out.println(kar);
		} else {
			kar = tekst.charAt(0);
			kar = Character.toLowerCase(kar);
			System.out.println(kar);
		}

	}

}
