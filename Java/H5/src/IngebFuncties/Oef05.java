/*
 * Schrijf een programma om van de personeelsleden van een school een personeelscode te maken van de achternaam en voornaam.
 * De code bestaat uit de eeerste 2 letters van de voornaam gevolgd door de eerste 2 letters van de achternaam.
 * BV. Koen Peeters OF KOEN PEETERS = KoPe
 * De namen worden ingegeven, de invoer stopt er als voor de achternaam ** ingegeven wordt.
 */
package IngebFuncties;

import Voorbeelden.Invoer;

public class Oef05 {

	public static void main(String[] args) {
		String voornaam, achternaam, code;
		voornaam = Invoer.leesString("Geef de voornaam in: ");
		achternaam = Invoer.leesString("Geef de achternaam in: ");
		while (!achternaam.equals("**")) {
			code = Character.toUpperCase(voornaam.charAt(0)) + ""
					+ Character.toLowerCase(voornaam.charAt(1)) + ""
					+ Character.toUpperCase(achternaam.charAt(0)) + ""
					+ Character.toLowerCase(achternaam.charAt(1));
			System.out.println("De personeelscode is: " + code);
			voornaam = Invoer.leesString("Geef de voornaam in: ");
			achternaam = Invoer.leesString("Geef de achternaam in: ");
		}

	}

}
