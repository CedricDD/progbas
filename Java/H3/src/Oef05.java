/*
 * Een verzekeringsmaatschappij verkoopt levensverzekeringen en berekent de te betalen maandpremie aan de hand van deze tabel:
 * leeftijd		maandpremie per 1250 euro
 * <20			3
 * >=20 <30		8
 * >=30 <40		18
 * >=40 < 60	25
 * >=60			37
 * 
 * De leeftijd van de verzekerde, alsook het te verzekeren bedrag (een veelvoud van 1250) wordt ingegeven.
 * Druk het te betalen bedrag per jaar af. Bv. Iemand van 28 en een bedrag 25000 euro = 1920 euro / jaar.
 */

public class Oef05 {

	public static void main(String[] args) {
		int leeftijd, verzBedrag, factor, jaarPr;
		leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
		verzBedrag = Invoer.leesInt("Geef het verzekerde bedrag in: ");
		factor = verzBedrag / 1250;
		if (leeftijd < 20) {
			jaarPr = factor * 3 * 12;
		} else if (leeftijd < 30) {
			jaarPr = factor * 8 * 12;
		} else if (leeftijd < 40) {
			jaarPr = factor * 18 * 12;
		} else if (leeftijd < 60) {
			jaarPr = factor * 25 * 12;
		} else {
			jaarPr = factor * 37 * 12;
		}
		System.out.println("De jaarpremie bedraagt: " + jaarPr + " euro");
	}
}
// switch case niet mogelijk!!!