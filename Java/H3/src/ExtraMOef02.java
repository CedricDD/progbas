/*
 * Lees 3 getallen a, b en c in. Deze getallen zijn reeds gerangschikt van klein naar groot. 
 * Lees een 4de getal in en druk de 3 kleinste getallen af, van klein naar groot.
 */

public class ExtraMOef02 {

	public static void main(String[] args) {
		int getalA, getalB, getalC, getalD;
		getalA = Invoer.leesInt("Geef het kleinste getal in: ");
		getalB = Invoer.leesInt("Geef het tweede kleinste getal in: ");
		getalC = Invoer.leesInt("Geef het grootste getal in; ");
		getalD = Invoer.leesInt("Geef een willekeurig getal in: ");
		if (getalD <= getalA) {
				System.out.println(getalD + ", " + getalA + ", " + getalB);
		}else{
			if(getalD <= getalB) {
				System.out.println(getalA + ", " + getalD + ", " + getalB);
			}else if (getalD <= getalC) {
				System.out.println(getalB + ", " + getalD + ", " + getalC);
			}else {
				System.out.println(getalA + ", " + getalB + ", " + getalC);
			}
			
		}
	}

}
