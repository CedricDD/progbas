/*
 * Autoverzekering: De prijs verandert elk jaar naargelang de bonus malus graad en het aanal schadegevallen.
 * Geen schadegevallen = daling bonus malus met 1. Bereken de bonus malus en druk deze af.
 * BM basis is 10, niet groter dan 18, niet kleiner dan 1. Het eerste ongeval = +2, alle andere = +3.
 * Druk af: naam, BM vorig jaar, aantal schadegevallen, BM dit jaar.
 */

public class ExtraMOef03 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String naam;
		int bmVj, aantOng, bmNieuw, aantOngJaar;
		naam = Invoer.leesString("Geef de naam in:  ");
		bmVj = Invoer.leesInt("Geef de bonus malus van vorig jaar in: ");
		aantOng = Invoer.leesInt("Geef het aantal ongevallen in: ");
		bmNieuw = bmVj;
		aantOngJaar = aantOng;
		if (aantOng == 0) {
			bmNieuw = bmNieuw - 1;
			}else if (aantOng == 1) {
						bmNieuw = bmNieuw + 2;
				}else {
					bmNieuw = (bmNieuw + 2 ) + ((aantOng - 1) * 3);
				}
		if (bmNieuw < 1) { // bonus malus mag niet minder dan 1 of meer dan 18 zijn
			bmNieuw = 1;
		} else if (bmNieuw > 18) {
			bmNieuw = 18;
		}
	System.out.println("Naam: " + naam);
	System.out.println("Bonus Malus vorig jaar: " + bmVj);
	System.out.println("Aantal Ongevallen: " + aantOngJaar);
	System.out.println("Bonus Malus dit jaar: " + bmNieuw);
	}
}
