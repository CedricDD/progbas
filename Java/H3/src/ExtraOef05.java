/*
 * Het jaarlijks lidgeld bij een biljartvereniging bedraagt:
 * 25 euro voor volwassenen, 12.5 euro voor personen onder de 18, 6 euro voor personen onder de 12.
 * Voor de leeftijd van een lid in, de leeftijd en het bedrag worden afgedrukt.
 */
public class ExtraOef05 {

	public static void main(String[] args) {
		int leeftijd;
		double prijs;
		leeftijd = Invoer.leesInt("Geef de leeftijd in: ");
		if (leeftijd < 12) {
			prijs = 6;
		} else {
			if (leeftijd < 18) {
				prijs = 12.5;
			}else {
				prijs = 25;
			}
		}
		System.out.println("De prijs bedraagt: " + prijs + " euro");
	}

}
