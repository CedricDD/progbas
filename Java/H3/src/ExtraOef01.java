/*
 * Geef 2 getallen a en b in, trek het kleinste van het grootste af en vermenigvuldig het resultaat met 5.
 */

public class ExtraOef01 {

	public static void main(String[] args) {
		int getalA, getalB, uitKomst = 0;
		getalA = Invoer.leesInt("Geef een getal in: ");
		getalB = Invoer.leesInt("Geef een getal in: ");
		if (getalA != getalB) {
			if (getalA < getalB) {
				uitKomst = (getalB - getalA) * 5;
				}else {
					uitKomst = (getalA - getalB) * 5;
				}
		}else {
			System.out.println("De getallen mogen niet gelijk zijn!");
		}
		System.out.println("De uitkomst is: " + uitKomst);
	}

}
